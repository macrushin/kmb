package factory;


import graphs.ListEdgesGraph;
import graphs.MatrixIncidenceGraph;
import graphs.NodeMapEdgesGraph;
import graphstructure.*;

import java.util.*;

public class GraphMaker {

    static public ListEdgesGraph getRandomListEdgesGraph(int countEdges, int countNodes, int minWeight, int maxWeight) throws Exception {
        List<Edge> edges = new ArrayList<>();
        int i = 0;
        while (i < countEdges) {
            int nodePrev = (int) (Math.random() * countNodes); // генерация числа от 0 до countNodes - 1
            int nodeNext;
            do {
                nodeNext = (int) (Math.random() * countNodes);
            } while (nodeNext == nodePrev);
            int randomWeight = (int) (Math.random() * (maxWeight - minWeight + 1) + minWeight); // генерация рандомного веса [minWeight, maxWeight] (целые числа)
            edges.add(new Edge(nodePrev, nodeNext, randomWeight));
            i++;
        }
        return new ListEdgesGraph(edges);
    }


    static public MatrixIncidenceGraph getRandomMatrixIncidenceGraph(int countEdges, int countNodes, int minWeight, int maxWeight) {
        int[][] matrixIncidence = new int[countNodes][countEdges];
        int i = 0;
        while (i < countEdges) {
            int nodePrevIndex = (int) (Math.random() * countNodes); // генерация числа от 0 до countNodes - 1
            int nodeNextIndex;
            do {
                nodeNextIndex = (int) (Math.random() * countNodes);
            } while (nodeNextIndex == nodePrevIndex);

            int randomWeight = (int) (Math.random() * (maxWeight - minWeight + 1) + minWeight); // генерация рандомного веса [minWeight, maxWeight] (целые числа)
            matrixIncidence[nodePrevIndex][i] = randomWeight;
            matrixIncidence[nodeNextIndex][i] = -randomWeight;
            i++;
        }
        return new MatrixIncidenceGraph(matrixIncidence);
    }


    static public NodeMapEdgesGraph getRandomNodeMapEdgesGraph(int countEdges, int countNodes, int minWeight, int maxWeight) {

        List<List<Edge>> nodeMapEdges = new ArrayList<>(countNodes);

        for (int currentNode = 0; currentNode < countNodes; currentNode++) {
            nodeMapEdges.add(currentNode, new ArrayList<>());
        }

        int i = 0;
        while (i < countEdges) {
            int nodePrev = (int) (Math.random() * countNodes); // генерация числа от 0 до countNodes - 1
            int nodeNext;
            do {
                nodeNext = (int) (Math.random() * countNodes);
            } while (nodeNext == nodePrev);


            int randomWeight = (int) (Math.random() * (maxWeight - minWeight + 1) + minWeight);// генерация рандомного веса [minWeight, maxWeight] (целые числа)
            nodeMapEdges.get(nodePrev).add(new Edge(nodePrev, nodeNext, randomWeight));
            i++;
        }
        return new NodeMapEdgesGraph(nodeMapEdges);
    }

}


