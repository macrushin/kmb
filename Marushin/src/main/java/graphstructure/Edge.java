package graphstructure;


public class Edge {

    private final int prev;

    private final int next;

    private int weight;

    public Edge(int prev, int next, int weight) {
        this.prev = prev;
        this.next = next;
        this.weight = weight;
    }

    public int getPrev() {
        return prev;
    }

    public int getNext() {
        return next;
    }

    public int getWeight() {
        return weight;
    }


    @Override
    public String toString() {
        return "{" +
                "prev=" + prev +
                ", next=" + next +
                ", weight=" + weight +
                '}';
    }
}
