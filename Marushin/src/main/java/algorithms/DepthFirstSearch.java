package algorithms;

import graphs.Graph;
import graphs.ListEdgesGraph;
import graphs.MatrixIncidenceGraph;
import graphstructure.Edge;

import graphs.NodeMapEdgesGraph;

import java.util.HashSet;
import java.util.Set;

public class DepthFirstSearch {

    public static boolean isLinked(Graph graph, Integer start, Integer finish) throws Exception {
        if (graph instanceof NodeMapEdgesGraph) {
            return isLinkedNodeMapEdgesGraph((NodeMapEdgesGraph) graph, start, finish, new HashSet<>());
        }
        if (graph instanceof ListEdgesGraph) {
            return isLinkedListEdgesGraph((ListEdgesGraph) graph, start, finish, new HashSet<>());
        }
        if (graph instanceof MatrixIncidenceGraph) {
            return isLinkedMatrixIncidenceGraph((MatrixIncidenceGraph) graph, start, finish, new HashSet<>());
        }
        throw new Exception("Не найден вид представления графа, по которому должен быть произведен обход");
    }

    private static boolean isLinkedMatrixIncidenceGraph(MatrixIncidenceGraph graph, Integer start, Integer finish, HashSet<Integer> visited) {
        if (start.equals(finish)) {
            return true;
        }
        visited.add(start);
        for (int startNodeInCurrentEdge=0; startNodeInCurrentEdge< graph.getMatrixIncidence()[start].length;startNodeInCurrentEdge++ ) {
            if (graph.getMatrixIncidence()[start][startNodeInCurrentEdge] <= 0) {
                continue;
            }
            for (int finishNodeInCurrentEdge = 0; finishNodeInCurrentEdge < graph.getMatrixIncidence().length; finishNodeInCurrentEdge++) {
                if (graph.getMatrixIncidence()[finishNodeInCurrentEdge][startNodeInCurrentEdge] < 0 && !visited.contains(finishNodeInCurrentEdge)) {
                    if(isLinkedMatrixIncidenceGraph(graph, finishNodeInCurrentEdge, finish, visited))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static boolean isLinkedListEdgesGraph(ListEdgesGraph graph, Integer start, Integer finish, HashSet<Integer> visited) {
        if (start.equals(finish)) {
            return true;
        }
        visited.add(start);
        for (Edge edge : graph.getEdges()) {
            if (start.equals(edge.getPrev()) && !visited.contains(edge.getNext())) {
                if (isLinkedListEdgesGraph(graph, edge.getNext(), finish, visited)) {
                    return true;
                }
            }
        }
        return false;
    }


    private static boolean isLinkedNodeMapEdgesGraph(NodeMapEdgesGraph graph, Integer start, Integer finish, Set<Integer> visited) {
        try {

            if (start.equals(finish)) {
                return true;
            }
            visited.add(start);
            for (Edge edge : graph.getNodeEdgesMapGraph().get(start)) {
                if (!visited.contains(edge.getNext())) {
                    if (isLinkedNodeMapEdgesGraph(graph, edge.getNext(), finish, visited)) {
                        return true;
                    }
                }
            }
            return false;
        }
        catch (StackOverflowError error)
        {error.printStackTrace();}
        return false;
    }
}
