package graphs;

import graphstructure.Edge;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.List;

public class NodeMapEdgesGraph extends Graph {
    @NotNull
    List<List<Edge>> nodeEdgesMapGraph;

    public NodeMapEdgesGraph(@NotNull List<List<Edge>> nodeEdgesMapGraph) {
        this.nodeEdgesMapGraph = nodeEdgesMapGraph;
    }

    @NotNull
    public List<List<Edge>> getNodeEdgesMapGraph() {
        return nodeEdgesMapGraph;
    }

    public void setNodeEdgesMapGraph(@NotNull List<List<Edge>> nodeEdgesMapGraph) {
        this.nodeEdgesMapGraph = nodeEdgesMapGraph;
    }

    public NodeMapEdgesGraph() {
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int nodePrev = 0; nodePrev < nodeEdgesMapGraph.size(); nodePrev++) {
            builder.append("nodePrev: ").append(nodePrev).append(" { ");
            Iterator<Edge> iterator = nodeEdgesMapGraph.get(nodePrev).listIterator();
            while (iterator.hasNext()) {
                Edge edge = iterator.next();
                if (iterator.hasNext()) {
                    builder.append(edge).append("; ");
                } else {
                    builder.append(edge);
                }
            }
            builder.append("}").append("\n");
        }
        return builder.toString();
    }

    public int getCountNodes() {
        return nodeEdgesMapGraph.size();
    }
    public void addEdge(Edge edge)
    {

    }

    public int getCountEdges() {
        int size = 0;
        for (List<Edge> edges : nodeEdgesMapGraph) {
            size += edges.size();
        }
        return size;
    }
}
