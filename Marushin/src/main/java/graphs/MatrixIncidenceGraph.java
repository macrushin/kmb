package graphs;


public class MatrixIncidenceGraph extends Graph {
    private int[][] matrixIncidence;

    public int[][] getMatrixIncidence() {
        return matrixIncidence;
    }

    public void setMatrixIncidence(int[][] matrixIncidence) {
        this.matrixIncidence = matrixIncidence;
    }

    public MatrixIncidenceGraph(int[][] matrixIncidence) {
        this.matrixIncidence = matrixIncidence;
    }

    public MatrixIncidenceGraph() {
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < matrixIncidence.length; i++) {
            for (int j = 0; j < matrixIncidence[i].length; j++) {
                builder.append(matrixIncidence[i][j]).append(", ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public int getCountNodes() {
        return matrixIncidence.length;
    }

    public int getCountEdges() {
        return matrixIncidence[0].length;
    }
}
