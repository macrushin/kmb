package graphs;

import graphstructure.Edge;

import java.util.*;

public class ListEdgesGraph extends Graph {

    private List<Edge> edges;


    public ListEdgesGraph(List<Edge> edges) {
        this.edges = edges;
    }


    public List<Edge> getEdges() {
        return edges;
    }

    public int getCountNodes() {
        Set<Integer> originalNodes = new HashSet<>();
        for (Edge edge : edges) {
            originalNodes.add(edge.getPrev());
            originalNodes.add(edge.getNext());
        }
        return originalNodes.size();

    }

    public void addEdge(Edge edge)
    {
        edges.add(edge);
    }


    public int getCountEdges() {
        return edges.size();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (Edge edge : edges) {
            builder.append(edge).append("\n");
        }
        return builder.toString();

    }
}

