package testgraph;

import algorithms.DepthFirstSearch;
import factory.GraphMaker;
import graphs.NodeMapEdgesGraph;
import org.junit.Test;
import java.util.logging.Logger;

public class DepthFirstSearchTest {
    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(DepthFirstSearchTest.class.getName());
    @Test
    public void testIsLinked() throws Exception {
        for (int countNodes = 100; countNodes <= 100000; countNodes *= 10) {
            NodeMapEdgesGraph nodeMapEdgesGraph1 = GraphMaker.getRandomNodeMapEdgesGraph(countNodes / 10, countNodes, 100, 1000);
            NodeMapEdgesGraph nodeMapEdgesGraph2 = GraphMaker.getRandomNodeMapEdgesGraph(countNodes / 2, countNodes, 100, 1000);
            NodeMapEdgesGraph nodeMapEdgesGraph3 = GraphMaker.getRandomNodeMapEdgesGraph(countNodes, countNodes, 100, 1000);
            NodeMapEdgesGraph nodeMapEdgesGraph4 = GraphMaker.getRandomNodeMapEdgesGraph(countNodes * 3, countNodes, 100, 1000);
            NodeMapEdgesGraph nodeMapEdgesGraph5 = GraphMaker.getRandomNodeMapEdgesGraph(countNodes * 200, countNodes, 100, 1000);
            long startTime = System.currentTimeMillis();
            DepthFirstSearch.isLinked(nodeMapEdgesGraph1, (int) (Math.random() * countNodes), (int) (Math.random() * countNodes));
            long endTime = System.currentTimeMillis();
            logger.info(String.valueOf(endTime - startTime));

            startTime = System.currentTimeMillis();
            DepthFirstSearch.isLinked(nodeMapEdgesGraph2, (int) (Math.random() * countNodes), (int) (Math.random() * countNodes));
            endTime = System.currentTimeMillis();
            logger.info(String.valueOf(endTime - startTime));
            startTime = System.currentTimeMillis();
            DepthFirstSearch.isLinked(nodeMapEdgesGraph3, (int) (Math.random() * countNodes), (int) (Math.random() * countNodes));
            endTime = System.currentTimeMillis();
            logger.info(String.valueOf(endTime - startTime));
            startTime = System.currentTimeMillis();
            DepthFirstSearch.isLinked(nodeMapEdgesGraph4, (int) (Math.random() * countNodes), (int) (Math.random() * countNodes));
            endTime = System.currentTimeMillis();
            logger.info(String.valueOf(endTime - startTime));
            startTime = System.currentTimeMillis();
            DepthFirstSearch.isLinked(nodeMapEdgesGraph5, (int) (Math.random() * countNodes), (int) (Math.random() * countNodes));
            endTime = System.currentTimeMillis();
            logger.info(String.valueOf(endTime - startTime));
        }
    }
}
