package testgraph;


import factory.GraphMaker;
import graphs.ListEdgesGraph;
import graphs.MatrixIncidenceGraph;
import graphs.NodeMapEdgesGraph;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GraphMakerTest {
    @Test
    public void testGetRandomGraphMatrixIncidence(){
        try {
            MatrixIncidenceGraph graphMatrixIncidence = GraphMaker.getRandomMatrixIncidenceGraph(
                    3,
                    5,
                    2,
                    100);
            assertEquals(graphMatrixIncidence.getCountNodes(), 5);
            assertEquals(graphMatrixIncidence.getCountEdges(), 3);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void testGetRandomGraphEdges(){
        try {
            ListEdgesGraph graphEdges = GraphMaker.getRandomListEdgesGraph(2, 5, 2, 100);
            assertEquals (graphEdges.getCountEdges() ,3);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Test
    public void testGetRandomGraphEdgesNodes(){
        try {
            NodeMapEdgesGraph graphSetNodes = GraphMaker.getRandomNodeMapEdgesGraph(3, 5, 2, 100);
            assertEquals(graphSetNodes.getCountNodes(), 5);
            assertEquals(graphSetNodes.getCountEdges(), 3);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
