package fabric;

import entity.AdjacencyMatrixGraph;
import entity.EdgeListGraph;
import entity.IncidenceMatrixGraph;
import entity.graph.Edge;
import entity.graph.Graph;
import enums.GraphTypes;

import java.util.ArrayList;
import java.util.List;

public class GraphFactory {

    public static Graph randomGraphFactory(GraphTypes type, int edgeCount, int nodeCount, long minWeight, long maxWeight) {
        Graph graph = null;
        switch (type) {
            case ADJACENCY_MATRIX:
                long[][] adjacencyMatrix = new long[nodeCount][nodeCount];
                for (int currentEdge = 0; currentEdge < edgeCount; currentEdge++) {
                    int currNode = (int) (Math.random() * nodeCount);
                    int nextNode;
                    do {
                        nextNode = (int) (Math.random() * nodeCount);
                    } while (currNode == nextNode);
                    adjacencyMatrix[currNode][nextNode] = (long) (Math.random() * (maxWeight - minWeight + 1) + minWeight);
                }
                graph = new AdjacencyMatrixGraph(adjacencyMatrix);
                break;

            case EDGE_LIST:
                List<Edge> edges = new ArrayList<>();
                for (int currEdge = 0; currEdge < edgeCount; currEdge++) {
                    int currNode = (int) (Math.random() * nodeCount);
                    int nextNode;
                    do {
                        nextNode = (int) (Math.random() * nodeCount);
                    } while (nextNode == currNode);
                    long randomWeight = (long) (Math.random() * (maxWeight - minWeight + 1) + minWeight);
                    edges.add(new Edge(currNode, nextNode, randomWeight));
                }
                graph = new EdgeListGraph(edges);
                break;

            case INCIDENCE_MATRIX:
                long[][] incidenceMatrix = new long[nodeCount][edgeCount];
                for (int currEdge = 0; currEdge < edgeCount; currEdge++) {
                    int currNode = (int) (Math.random() * nodeCount);
                    int nextNode;
                    do {
                        nextNode = (int) (Math.random() * nodeCount);
                    } while (nextNode == currNode);
                    long randomWeight = (long) (Math.random() * (maxWeight - minWeight + 1) + minWeight);
                    incidenceMatrix[currNode][currEdge] = randomWeight;
                    incidenceMatrix[nextNode][currEdge] = -randomWeight;
                }
                graph = new IncidenceMatrixGraph(incidenceMatrix);
                break;
        }
        return graph;
    }

    public static Graph graphFactory(GraphTypes type, int nodeCount) {
        Graph graph = null;
        switch (type) {
            case ADJACENCY_MATRIX:
                long[][] adjacencyMatrix = new long[nodeCount][nodeCount];
                graph = new AdjacencyMatrixGraph(adjacencyMatrix);
                break;
            case EDGE_LIST:
                List<Edge> edges = new ArrayList<>();
                graph = new EdgeListGraph(edges);
                break;
            case INCIDENCE_MATRIX:
                long[][] incidenceMatrix = new long[nodeCount][0];
                graph = new IncidenceMatrixGraph(incidenceMatrix);
                break;
        }
        return graph;
    }

}
