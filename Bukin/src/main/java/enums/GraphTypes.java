package enums;

public enum GraphTypes {

    ADJACENCY_MATRIX,
    EDGE_LIST,
    INCIDENCE_MATRIX

}
