package entity;

import entity.graph.Edge;
import entity.graph.Graph;

import java.util.Arrays;

public class IncidenceMatrixGraph implements Graph {

    private final long[][] incidenceMatrix;

    public IncidenceMatrixGraph(long[][] incidenceMatrix) {
        this.incidenceMatrix = incidenceMatrix;
    }

    public long[][] getIncidenceMatrix() {
        return incidenceMatrix;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Incidence matrix graph").append('\n');
        for (long[] matrix : incidenceMatrix) {
            for (long i : matrix) {
                builder.append(i).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    @Override
    public void addEdge(Edge edge) {
        for (int i = 0; i < incidenceMatrix.length; i++)
            incidenceMatrix[i] = Arrays.copyOf(incidenceMatrix[i], incidenceMatrix[i].length + 1);
        incidenceMatrix[edge.getFromNode()][incidenceMatrix[edge.getFromNode()].length - 1] = edge.getWeight();
        incidenceMatrix[edge.getToNode()][incidenceMatrix[edge.getToNode()].length - 1] = -edge.getWeight();
    }

}
