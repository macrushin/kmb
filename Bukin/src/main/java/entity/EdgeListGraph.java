package entity;

import entity.graph.Edge;
import entity.graph.Graph;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EdgeListGraph implements Graph {

    @NotNull
    private final List<Edge> edgeList;

    public EdgeListGraph(@NotNull List<Edge> edgeList) {
        this.edgeList = edgeList;
    }

    @NotNull
    public List<Edge> getEdgeList() {
        return edgeList;
    }

    public int getNodeCount() {
        Set<Integer> nodes = new HashSet<>();
        for (Edge edge : edgeList) {
            nodes.add(edge.getFromNode());
            nodes.add(edge.getToNode());
        }
        int maxNode = 0;
        for (Integer node : nodes) {
            if (maxNode < node)
                maxNode = node;
        }
        return maxNode + 1;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Edge list graph").append('\n');
        for (Edge edge : edgeList)
            builder.append(edge.getFromNode()).append(" ").append(edge.getToNode()).append(" ").append(edge.getWeight()).append('\n');
        return builder.toString();
    }

    @Override
    public void addEdge(Edge edge) {
        edgeList.add(edge);
    }

}
