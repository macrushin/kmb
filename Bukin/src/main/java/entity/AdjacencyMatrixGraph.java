package entity;

import entity.graph.Edge;
import entity.graph.Graph;

public class AdjacencyMatrixGraph implements Graph {

    private final long[][] adjacencyMatrix;

    public AdjacencyMatrixGraph(long[][] adjacencyMatrix) {
        this.adjacencyMatrix = adjacencyMatrix;
    }

    public long[][] getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Adjacency matrix graph").append('\n');
        for (long[] matrix : adjacencyMatrix) {
            for (long i : matrix) {
                builder.append(i).append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    @Override
    public void addEdge(Edge edge) {
        adjacencyMatrix[edge.getFromNode()][edge.getToNode()] = edge.getWeight();
    }

}
