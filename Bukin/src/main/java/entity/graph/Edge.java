package entity.graph;

public class Edge {

    private final int fromNode;
    private final int toNode;
    private final long weight;

    public Edge(int fromNode, int toNode, long weight) {
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.weight = weight;
    }

    public int getFromNode() {
        return fromNode;
    }

    public int getToNode() {
        return toNode;
    }

    public long getWeight() {
        return weight;
    }

}
