package algorithm;


import entity.AdjacencyMatrixGraph;
import entity.EdgeListGraph;
import entity.IncidenceMatrixGraph;
import entity.graph.Edge;
import entity.graph.Graph;
import enums.NodeStates;

import java.util.Stack;

public class CheckPathGraph {

    public static boolean isLinked(Graph graph, int startNode, int endNode) {
        if (graph instanceof AdjacencyMatrixGraph)
            return isLinkedNodesAdjacencyMatrixGraph((AdjacencyMatrixGraph) graph, startNode, endNode);
        if (graph instanceof IncidenceMatrixGraph)
            return isLinkedNodesIncidenceMatrix((IncidenceMatrixGraph) graph, startNode, endNode);
        if (graph instanceof EdgeListGraph)
            return isLinkedNodesEdgeList((EdgeListGraph) graph, startNode, endNode);
        throw new RuntimeException("Incorrect graph type");
    }

    private static boolean isLinkedNodesAdjacencyMatrixGraph(AdjacencyMatrixGraph adjacencyMatrixGraph, int startNode, int endNode) {
        Stack<Integer> stack = new Stack<>();
        int nodeCount = adjacencyMatrixGraph.getAdjacencyMatrix().length;
        NodeStates[] nodesStates = new NodeStates[nodeCount];
        stack.push(startNode);
        while (!stack.empty()) {
            int currentNode = stack.pop();
            if (nodesStates[currentNode] == NodeStates.VISITED)
                continue;
            nodesStates[currentNode] = NodeStates.VISITED;
            for (int toNode = 0; toNode < nodeCount; toNode++) {
                if (adjacencyMatrixGraph.getAdjacencyMatrix()[currentNode][toNode] != 0) { // ребро из currentNode в toNode
                    if (nodesStates[toNode] != null) // вершина на другом конце ребра уже посещена или обнаружена
                        continue;
                    if (toNode == endNode)
                        return true;
                    stack.push(toNode);
                    nodesStates[toNode] = NodeStates.QUEUED;
                }
            }
        }
        return false;
    }

    private static boolean isLinkedNodesIncidenceMatrix(IncidenceMatrixGraph incidenceMatrixGraph, int startNode, int endNode) {
        Stack<Integer> stack = new Stack<>();
        int nodeCount = incidenceMatrixGraph.getIncidenceMatrix().length;
        int edgeCount = incidenceMatrixGraph.getIncidenceMatrix()[startNode].length;
        NodeStates[] nodesStates = new NodeStates[nodeCount];
        stack.push(startNode);
        while (!stack.empty()) {
            int currentNode = stack.pop();
            if (nodesStates[currentNode] == NodeStates.VISITED)
                continue;
            nodesStates[currentNode] = NodeStates.VISITED;
            final long[] edges = incidenceMatrixGraph.getIncidenceMatrix()[currentNode]; //список ребер вершины currentNode
            for (int currentEdge = 0; currentEdge < edgeCount - 1; currentEdge++) {
                if (edges[currentEdge] <= 0) // не исходящее из currentNode ребро
                    continue;
                for (int toNode = 0; toNode < nodeCount; toNode++) {
                    if (incidenceMatrixGraph.getIncidenceMatrix()[toNode][currentEdge] < 0) { // ребро из currentNode в toNode
                        if (nodesStates[toNode] != null) // вершина на другом конце ребра уже посещена или обнаружена
                            continue;
                        if (toNode == endNode)
                            return true;
                        stack.push(toNode);
                        nodesStates[toNode] = NodeStates.QUEUED;
                        break;
                    }
                }
            }
        }
        return false;
    }

    private static boolean isLinkedNodesEdgeList(EdgeListGraph edgeListGraph, int startNode, int endNode) {
        Stack<Integer> stack = new Stack<>();
        NodeStates[] nodesStates = new NodeStates[edgeListGraph.getNodeCount()];
        stack.push(startNode);
        while (!stack.empty()) {
            int currentNode = stack.pop();
            if (nodesStates[currentNode] == NodeStates.VISITED)
                continue;
            nodesStates[currentNode] = NodeStates.VISITED;
            for (Edge edge : edgeListGraph.getEdgeList()) {
                if (currentNode == edge.getFromNode()) { // ребро из currentNode
                    if (nodesStates[edge.getToNode()] != null) // вершина на другом конце ребра уже посещена или обнаружена
                        continue;
                    if (edge.getToNode() == endNode)
                        return true;
                    stack.push(edge.getToNode());
                    nodesStates[edge.getToNode()] = NodeStates.QUEUED;
                }
            }
        }
        return false;
    }

}
