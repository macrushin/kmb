package algorithm;

import entity.graph.Graph;
import enums.GraphTypes;
import fabric.GraphFactory;
import org.jetbrains.annotations.NotNull;

public class CheckPerformancePathGraph {

    private static final String STRING_FORMAT = "|%-20s|";
    private static final String INT_FORMAT = "|%-20s|";
    private static boolean isLinked;

    @NotNull
    public static String checkPerformanceAdjacencyMatrixGraphPath(int edgeCount, int nodeCount, int startNode, int endNode) {
        Graph graph = GraphFactory.randomGraphFactory(GraphTypes.ADJACENCY_MATRIX, edgeCount, nodeCount, 1, 9);
        return getString(edgeCount, nodeCount, startNode, endNode, graph);
    }

    @NotNull
    public static String checkPerformanceIncidenceMatrixGraphPath(int edgeCount, int nodeCount, int startNode, int endNode) {
        Graph graph = GraphFactory.randomGraphFactory(GraphTypes.INCIDENCE_MATRIX, edgeCount, nodeCount, 1, 9);
        return getString(edgeCount, nodeCount, startNode, endNode, graph);
    }

    @NotNull
    public static String checkPerformanceEdgeListGraphPath(int edgeCount, int nodeCount, int startNode, int endNode) {
        Graph graph = GraphFactory.randomGraphFactory(GraphTypes.EDGE_LIST, edgeCount, nodeCount, 1, 9);
        return getString(edgeCount, nodeCount, startNode, endNode, graph);
    }

    @NotNull
    private static String getString(int edgeCount, int nodeCount, int startNode, int endNode, Graph graph) {
        StringBuilder output = inputDataToString(nodeCount, edgeCount);

        long timeMark = getTimeMark(graph, startNode, endNode);
        String timeMarkResult = String.format(STRING_FORMAT, timeMark + " ms");
        output.append(timeMarkResult);

        String isLinkedResult = String.format(STRING_FORMAT, isLinked);
        output.append(isLinkedResult);

        return output.toString();
    }

    @NotNull
    private static StringBuilder inputDataToString(int nodeCount, int edgeCount) {
        StringBuilder inputData = new StringBuilder();
        String result = String.format(INT_FORMAT, nodeCount);
        inputData.append(result);
        result = String.format(INT_FORMAT, edgeCount);
        inputData.append(result);
        return inputData;
    }

    private static long getTimeMark(Graph graph, int startNode, int endNode) {
        long startTime = System.currentTimeMillis();
        isLinked = CheckPathGraph.isLinked(graph, startNode, endNode);
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    @NotNull
    public static String makeCols() {
        StringBuilder colsName = new StringBuilder();
        String result = String.format(STRING_FORMAT, "Node count");
        colsName.append(result);
        result = String.format(STRING_FORMAT, "Edge count");
        colsName.append(result);
        result = String.format(STRING_FORMAT, "Computational time");
        colsName.append(result);
        result = String.format(STRING_FORMAT, "isLinked result");
        colsName.append(result);
        return colsName.toString();
    }

}
