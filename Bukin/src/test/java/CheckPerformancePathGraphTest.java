import algorithm.CheckPerformancePathGraph;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class CheckPerformancePathGraphTest {

    @Test
    public void checkPerformanceAdjacencyMatrixGraphPath1() {
        log.info("\t\t\t\t\t\t\t\tAdjacency matrix");
        int nodeCount = 100;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceAdjacencyMatrixGraphPath2() {
        log.info("\t\t\t\t\t\t\t\tAdjacency matrix");
        int nodeCount = 1000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceAdjacencyMatrixGraphPath3() {
        log.info("\t\t\t\t\t\t\t\tAdjacency matrix");
        int nodeCount = 10000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));

    }

    @Test
    public void checkPerformanceAdjacencyMatrixGraphPath4() {
        log.info("\t\t\t\t\t\t\t\tAdjacency matrix");
        int nodeCount = 100000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceAdjacencyMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceIncidenceMatrixGraphPathTest1() {
        log.info("\t\t\t\t\t\t\t\tIncidence matrix");
        int nodeCount = 100;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceIncidenceMatrixGraphPathTest2() {
        log.info("\t\t\t\t\t\t\t\tIncidence matrix");
        int nodeCount = 1000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceIncidenceMatrixGraphPathTest3() {
        log.info("\t\t\t\t\t\t\t\tIncidence matrix");
        int nodeCount = 10000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));

    }

    @Test
    public void checkPerformanceIncidenceMatrixGraphPathTest4() {
        log.info("\t\t\t\t\t\t\t\tIncidence matrix");
        int nodeCount = 100000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceIncidenceMatrixGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceEdgeListGraphPathTest1() {
        log.info("\t\t\t\t\t\t\t\tEdge list");
        int nodeCount = 100;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceEdgeListGraphPathTest2() {
        log.info("\t\t\t\t\t\t\t\tEdge list");
        int nodeCount = 1000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceEdgeListGraphPathTest3() {
        log.info("\t\t\t\t\t\t\t\tEdge list");
        int nodeCount = 10000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

    @Test
    public void checkPerformanceEdgeListGraphPathTest4() {
        log.info("\t\t\t\t\t\t\t\tEdge list");
        int nodeCount = 100000;
        int startNode = 0; //(int) (Math.random() * nodeCount)
        int endNode = nodeCount - 1; //(int) (Math.random() * nodeCount)
        log.info(CheckPerformancePathGraph.makeCols());
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 10, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount / 2, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 3, nodeCount, startNode, endNode));
        log.info(CheckPerformancePathGraph.checkPerformanceEdgeListGraphPath(nodeCount * 200, nodeCount, startNode, endNode));
    }

}
