import algorithm.CheckPathGraph;
import entity.graph.Edge;
import entity.graph.Graph;
import enums.GraphTypes;
import fabric.GraphFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GraphFactoryTest {

    @Test
    public void isLinkedNodesAdjacencyMatrixGraphTest() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.ADJACENCY_MATRIX, 5);
        graph.addEdge(new Edge(3, 0, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        graph.addEdge(new Edge(1, 2, 6));
        assertTrue(CheckPathGraph.isLinked(graph, 1, 0));
    }

    @Test
    public void isLinkedNodesAdjacencyMatrixGraphTest2() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.ADJACENCY_MATRIX, 5);
        graph.addEdge(new Edge(0, 3, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        graph.addEdge(new Edge(1, 2, 6));
        assertFalse(CheckPathGraph.isLinked(graph, 1, 0));
    }

    @Test
    public void isLinkedNodesEdgeListGraphTest() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.EDGE_LIST, 5);
        graph.addEdge(new Edge(3, 0, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        graph.addEdge(new Edge(1, 2, 6));
        assertTrue(CheckPathGraph.isLinked(graph, 1, 0));
    }

    @Test
    public void isLinkedNodesEdgeListGraphTest2() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.EDGE_LIST, 5);
        graph.addEdge(new Edge(0, 3, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        assertFalse(CheckPathGraph.isLinked(graph, 1, 0));
    }

    @Test
    public void isLinkedNodesIncidenceMatrixGraphTest() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.INCIDENCE_MATRIX, 5);
        graph.addEdge(new Edge(3, 0, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        graph.addEdge(new Edge(1, 2, 6));
        assertTrue(CheckPathGraph.isLinked(graph, 1, 0));
    }

    @Test
    public void isLinkedNodesIncidenceMatrixGraphTest2() {
        Graph graph = GraphFactory.graphFactory(GraphTypes.INCIDENCE_MATRIX, 5);
        graph.addEdge(new Edge(0, 3, 9));
        graph.addEdge(new Edge(1, 4, 5));
        graph.addEdge(new Edge(2, 3, 3));
        graph.addEdge(new Edge(0, 4, 7));
        graph.addEdge(new Edge(2, 4, 1));
        graph.addEdge(new Edge(4, 3, 2));
        assertFalse(CheckPathGraph.isLinked(graph, 1, 0));
    }

}
